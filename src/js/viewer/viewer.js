export default class Viewer {
    constructor() {
        this.init();
    }

    init() {
        // CONTAINER
        this.container = document.createElement('div');
        this.container.id = 'threeJS';
        document.body.appendChild(this.container);

        // RENDERER
        this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        // this.renderer = new THREE.WebGLRenderer();
        // this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        // SCENE
        window.scene = this.scene = new THREE.Scene();

        // this.scene.fog = new THREE.Fog(0xD0D0D0, 10, 180);

        // CAMERA
        this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 20000);
        this.camera.position.set(0, 14, 47);
        window.camera = this.camera;

        // CONTROLS
        this.initControls();

        // LIGHT
        this.initLight();

        // ADD ON SCENE
        this.container.appendChild(this.renderer.domElement);

        // RESIZE EVENT
        window.addEventListener('resize', this.onWindowResize.bind(this));

        // postprocessing
        // this.initCandleLight();
        this.animate();
    }

    initCandleLight() {
        this.composer = new THREE.EffectComposer(this.renderer);
        this.renderPass = new THREE.RenderPass(this.scene, this.camera);
        this.composer.addPass(this.renderPass);

        this.outlinePass = new THREE.OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this.scene, this.camera);
        this.outlinePass.selectedObjects = [];
        this.composer.addPass(this.outlinePass);

        this.effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
        this.effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);
        this.composer.addPass(this.effectFXAA);
    }

    initControls() {
        window.c = this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.maxPolarAngle = 1.33;
        this.controls.minPolarAngle = 1.33;
        this.controls.dampingFactor = 0.1;
        this.controls.rotateSpeed = 0.04;
        this.controls.enableDamping = true;
        this.controls.enableZoom = false;
        this.controls.panSpeed = 0.04;
        this.controls.enableKeys = false;
        this.controls.enablePan = true;
        this.controls.target.y = -3.5;
        this.controls.maxDistance = 50;
        this.controls.minDistance = 49.5;

        this.mouse = {x: 0, y: 0};

        document.addEventListener('mousedown', (event) => {
            if (event.button === 2) {
                this.controls.enablePan = false;
                this.speacilPanMode = true;
                this.mouse = {
                    x: event.layerX,
                    y: event.layerY
                }
            }
        });

        document.addEventListener('mouseup', (event) => {
            if (event.button === 2) {
                this.controls.enablePan = true;
                this.speacilPanMode = false;
                this.xCoff = 0;
                this.yCoff = 0;
            }

        });

        document.addEventListener('mousemove', (e) => {
            if (this.speacilPanMode) {
                const x = e.layerX;
                const y = e.layerY;

                if (x > this.mouse.x) {
                    this.xCoff = (x - this.mouse.x) / 600 * 1.1;

                    this.right = true;
                    this.left = false;
                } else {
                    this.xCoff = (this.mouse.x - x) / 600 * 1.1;

                    this.right = false;
                    this.left = true;
                }

                if (y > this.mouse.y) {
                    this.yCoff = (y - this.mouse.y) / 500 * 1.2;
                    this.top = true;
                    this.down = false;

                } else {
                    this.yCoff = (this.mouse.y - y) / 500 * 1.2;

                    this.top = false;
                    this.down = true;
                }


            }
        });

        document.addEventListener('keydown', (e) => {
            if (e.code === 'ArrowRight') {
                this.xCoffArrow = 1;
                this.right = true;
                this.left = false;
            }
            if (e.code === 'ArrowLeft') {
                this.xCoffArrow = 1;
                this.right = false;
                this.left = true;
            }
            if (e.code === 'ArrowUp') {
                this.yCoffArrow = 1;
                this.top = false;
                this.down = true;
            }
            if (e.code === 'ArrowDown') {
                this.yCoffArrow = 1;
                this.top = true;
                this.down = false;
            }

        });
        document.addEventListener('keyup', (e) => {

            if (e.code === 'ArrowRight') {
                this.xCoffArrow = 0;
            }
            if (e.code === 'ArrowLeft') {
                this.xCoffArrow = 0;
            }
            if (e.code === 'ArrowUp') {
                this.yCoffArrow = 0;
            }
            if (e.code === 'ArrowDown') {
                this.yCoffArrow = 0;
            }

        });
    }

    initLight() {
        // let light = new THREE.HemisphereLight( 0xffffff, 0x999999, 2.15 );
        let light = new THREE.HemisphereLight(0xffffff, 0xbababa, 2);
        light.name = 'HemisphereLight';
        light.position.set(0, 300, 0);
        this.scene.add(light);

        let dlight1 = new THREE.DirectionalLight(0xffffff, 0.25);
        dlight1.name = 'DirectionalLight1';
        dlight1.position.set(300, 300, 0);
        this.scene.add(dlight1);

        let dlight2 = new THREE.DirectionalLight(0xffffff, 0.15);
        dlight2.name = 'DirectionalLight2';
        dlight2.position.set(-100, 300, 300);
        this.scene.add(dlight2);

        let dlight3 = new THREE.DirectionalLight(0xffffff, 0.1);
        dlight3.name = 'DirectionalLight3';
        dlight3.position.set(-100, 300, -300);
        this.scene.add(dlight3);
    }

    onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    animate() {
        requestAnimationFrame(() => this.animate());

        if (this.speacilPanMode) {

            if (this.xCoff > 0) {
                if (this.left)
                    this.controls.specialPan('left', this.xCoff);
                else if (this.right)
                    this.controls.specialPan('right', this.xCoff);
            }
            if (this.yCoff > 0) {
                if (this.top)
                    this.controls.specialPan('top', this.yCoff);
                else if (this.down)
                    this.controls.specialPan('down', this.yCoff);
            }

        }


        if (this.xCoffArrow > 0) {
            if (this.left)
                this.controls.specialPan('left', this.xCoffArrow);
            else if (this.right)
                this.controls.specialPan('right', this.xCoffArrow);
        }
        if (this.yCoffArrow > 0) {
            if (this.top)
                this.controls.specialPan('top', this.yCoffArrow);
            else if (this.down)
                this.controls.specialPan('down', this.yCoffArrow);
        }


        TWEEN.update();
        this.controls.update();
        this.renderer.render(this.scene, this.camera);
    }
}
