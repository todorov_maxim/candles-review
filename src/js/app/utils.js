import axios from 'axios';

// const url = 'http://localhost:3001';
const url = 'https://gexart.com';


export default class Utils {

    constructor() {

    }

    getData() {
        return axios.get(url + '/api/');
    }

    postData(coord, data) {

        const json = {
            payment: parseInt(data.amount),
            email: data.email,
            name: data.name,
            message: data.memo,
            coord: {x: coord.x, y: coord.y, z: coord.z}
        };


        axios.post(url + '/api/', json)
            .then(function (response) {
                console.log(response);
            })
    }

    deleteData(id){
        return axios.delete(url + '/api/' + id);
    }

    getAbout(){
        return axios.get(url + '/api/about');
    }

}
