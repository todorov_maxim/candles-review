import Utils from './utils';
import MAP_Candles from '../../assets/img/1.jpg';
import MAP_CandleShadow from '../../assets/img/candle-shadow.jpg';

export default class PlaneComponent {

    constructor(viewer) {
        this.viewer = viewer;
        this.utils = new Utils();
        this.candleList = [];
        this.wrapSpheres = new THREE.Object3D();
        this.wrapSpheres.position.y = -3.5;
        this.wrapSpheres.rotateX(-Math.PI / 2);
        this.viewer.scene.add(this.wrapSpheres);

        new THREE.ImageLoader().load(MAP_Candles);
        new THREE.ImageLoader().load(MAP_CandleShadow);

        this.initPlane();

        window.flyTo = (id) => this.flyToCandle(id);
    }

    initPlane() {
        this.plane = new THREE.Mesh(
            new THREE.CircleGeometry(5000, 32),
            new THREE.MeshBasicMaterial({color: 0x00ff00, visible: false})
        );

        this.plane.rotateX(-Math.PI / 2);
        this.plane.position.set(0, -3.5, 0);

        window.p = this.plane;

        this.viewer.scene.add(this.plane);
    }

    addShadow(candle) {
        const candleShadow = new THREE.Mesh(
            new THREE.CircleGeometry(0.7, 32),
            new THREE.MeshBasicMaterial({
                color: 0xffffff,
                opacity: 0.2,
                transparent: true,
                map: new THREE.TextureLoader().load(MAP_CandleShadow)
            })
        );
        candleShadow.position.copy(candle.position);
        candleShadow.position.z -= candle.geometry.boundingBox.getSize(new THREE.Vector3()).y / 2;
        this.plane.add(candleShadow);
    }

    flyToCandle(candleId, fast) {
        const candle = this.candleList.find(candle => candle.userData.id == candleId);

        if (candle) {
            const startTarget = this.viewer.controls.target.clone();
            const endTarget = candle.position.clone();
            endTarget.z = 0;
            endTarget.copy(this.wrapSpheres.localToWorld(endTarget.clone()));

            new TWEEN.Tween({alpha: 0})
                .to({alpha: 1}, fast ? 0 : 1100)
                .onUpdate((delta) => {
                    this.viewer.controls.target.copy(startTarget.clone().lerp(endTarget, delta));
                })
                .start();
        }

        return candle;
    }

    addСandle(intersect, candleData, candleId) {
        if (!candleData.server)
            this.utils.postData(intersect.point, candleData);

        const candleHeight = Math.sqrt(candleData.amount);
        // const candleHeight = 4 * (candleData.amount / 2.5);

        const cylinder = new THREE.Mesh(
            new THREE.CylinderGeometry(0.2, 0.2, candleHeight, 32),
            new THREE.MeshStandardMaterial({color: 0xffffff, map: new THREE.TextureLoader().load(MAP_Candles)})
        );
        cylinder.material.map.wrapT = THREE.RepeatWrapping;
        cylinder.material.map.repeat.y = candleHeight / 10;
        cylinder.material.map.offset.y = Math.random();
        cylinder.geometry.computeBoundingBox();
        cylinder.rotateX(Math.PI / 2);
        cylinder.position.copy(this.plane.worldToLocal(intersect.point.clone()));
        cylinder.position.z += cylinder.geometry.boundingBox.getSize(new THREE.Vector3()).y / 2;
        cylinder.userData.id = candleId;
        cylinder.userData.name = candleData.name;
        cylinder.userData.memo = candleData.memo;
        cylinder.userData.email = candleData.email;
        cylinder.userData.amount = candleData.amount;


        const cylinder2 = new THREE.Mesh(
            new THREE.CylinderGeometry(0.2, 0.2, candleHeight, 32),
            new THREE.MeshStandardMaterial({color: 0xfcffc8, transparent: true, opacity: 0.45, side: 1})
        );
        cylinder2.position.set(cylinder.position.x, cylinder.position.y, cylinder.position.z);
        cylinder2.rotateX(Math.PI / 2);
        cylinder2.scale.set(1.48, 1.00, 1.48);


        this.addShadow(cylinder);
        this.candleList.push(cylinder);
        this.plane.add(cylinder);
        this.plane.add(cylinder2);

    }
}
