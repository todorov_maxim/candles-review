import Viewer from '../viewer/viewer';
import PlaneComponent from './PlaneComponent';
import Utils from './utils';


export default class App {

    constructor() {
        this.viewer = new Viewer();
        this.planeComponent = new PlaneComponent(this.viewer);
        this.utils = new Utils();
        this.raycaster = new THREE.Raycaster();
        this.allowCreate = false;
        this.wrapMemoText = document.getElementById("wrap-memo-text");
        this.memoName = document.getElementById("memo-name");
        this.memoText = document.getElementById("memo-text");
        this.threeJS = document.getElementById("threeJS");
        this.windowAbout = document.getElementById("window-about");
        this.aboutDescr = document.getElementById("about-descr");
        this.newCandleData = null;
        this.dblClickEvent = null;

        window.a = this;

        this.mouse = new THREE.Vector2();

        this.utils.getAbout().then((res) => {
            let aboutDescrText = '';
            res.data.about.forEach( row => aboutDescrText += (row + '<br><br>'));
            this.aboutDescr.innerHTML = aboutDescrText;
        });

        if(window.location.search.includes('remove_id')) {
            const id = window.location.search.split('remove_id=')[1];
            this.utils.deleteData(id).then(() => {
                this.utils.getData().then((res) => {
                    const data = res.data;

                    for (let i = 0; i < data.length; i++) {
                        const vector3 = new THREE.Vector3(data[i].coord.x, data[i].coord.y, data[i].coord.z);
                        this.planeComponent.addСandle({point: vector3}, {
                            amount: data[i].payment,
                            email: data[i].email,
                            memo: data[i].message,
                            name: data[i].name,
                            server: true
                        }, data[i]._id);
                    }
                });
            });
        }
        else if(window.location.search.includes('id')) {
            const id = window.location.search.split('id=')[1];
            // fly to... Need to do.

            this.utils.getData().then((res) => {
                const data = res.data;

                for (let i = 0; i < data.length; i++) {
                    const vector3 = new THREE.Vector3(data[i].coord.x, data[i].coord.y, data[i].coord.z);
                    this.planeComponent.addСandle({point: vector3}, {
                        amount: data[i].payment,
                        email: data[i].email,
                        memo: data[i].message,
                        name: data[i].name,
                        server: true
                    }, data[i]._id);
                }

                this.planeComponent.flyToCandle(id, true);
            });
        }
        else {
            this.utils.getData().then((res)=>{
                const data = res.data;

                for (let i = 0; i < data.length; i++) {
                    const vector3 = new THREE.Vector3(data[i].coord.x, data[i].coord.y, data[i].coord.z);
                    this.planeComponent.addСandle({point: vector3}, {
                        amount: data[i].payment,
                        email: data[i].email,
                        memo: data[i].message,
                        name: data[i].name,
                        server: true
                    }, data[i]._id);
                }
            });
        }


        document.addEventListener('click', (event) => {
            if (event.target.id === 'memoweb') this.onClickMemoweb();
            if (event.target.id === 'add-candle') this.showModalAddCandle();
            if (event.target.id === 'about') this.showAbout();
            if (event.target.id === 'search') this.showModalSearch();
            if (event.target.classList.contains('close-modal')) {
                this.dblClickEvent = null;
                this.closeModal(event.target);
            }
        });
        document.addEventListener('touchstart', (event) => {
            if (event.target.id === 'add-candle') this.showModalAddCandle();
            if (event.target.id === 'search') this.showModalSearch();
            if (event.target.classList.contains('close-modal')) {
                this.dblClickEvent = null;
                this.closeModal(event.target);
            }
        });

        document.addEventListener('dblclick', (event) => {
            if (document.body.classList.contains('hideCanvas')) return;

            if (!this.dblClickEvent && this.allowCreate) this.newCandle(event.x, event.y);
            else {
                this.dblClickEvent = event;
                this.showModalAddCandle();
            }
        });
        document.addEventListener('touchend', (event) => {
            if (document.body.classList.contains('hideCanvas')) return;

            const touchPos = event.changedTouches[0];
            if (!this.dblClickEvent && event.target.tagName === "CANVAS") this.newCandle(touchPos.clientX, touchPos.clientY);
        });

        document.getElementById("new-candle-data").addEventListener("submit", (event) => this.submitSaveCandle(event));
        document.getElementById("search-candle").addEventListener("submit", (event) => this.submitSearchCandle(event));

        document.addEventListener('mousemove', (event) => {
            if (document.body.classList.contains('hideCanvas')) return;

            this.mouse.x = (event.layerX / this.viewer.renderer.domElement.offsetWidth) * 2 - 1;
            this.mouse.y = -(event.layerY / this.viewer.renderer.domElement.offsetHeight) * 2 + 1;

            this.raycaster.setFromCamera(this.mouse, this.viewer.camera);
            const intersects = this.raycaster.intersectObjects(this.planeComponent.candleList);

            if (intersects.length) {
                this.wrapMemoText.style.display = 'block';
                this.wrapMemoText.style.transform = `translate(${event.layerX}px,${event.layerY}px)`;
                this.memoName.innerText = intersects[0].object.userData.name;
                this.memoText.innerText = intersects[0].object.userData.memo;
            } else {
                this.wrapMemoText.style.display = 'none';
            }
        });
    }

    onClickMemoweb() {
        this.windowAbout.classList.remove('show');
        this.showCanvas(true);
    }

    showCanvas(isShow) {
        if (isShow === undefined || isShow === true) {
            this.windowAbout.classList.remove('show');
            document.body.classList.remove('hideCanvas');
        }
        else document.body.classList.add('hideCanvas');
    }

    showAbout() {
        this.windowAbout.classList.add('show');
        this.showCanvas(false);
    }

    submitSearchCandle(event) {
        event.preventDefault();
        if (!this.planeComponent.flyToCandle(event.target['id-candle'].value)) {
            document.getElementById('search-candle').classList.add('error');
        } else this.closeModal();

        event.target.reset();
    }

    submitSaveCandle(event) {
        event.preventDefault();
        this.closeModal(event.target);

        this.newCandleData = {};
        this.newCandleData.name = event.target['name-candle'].value;
        this.newCandleData.memo = event.target['memo-candle'].value;
        this.newCandleData.email = event.target['email-candle'].value;
        this.newCandleData.amount = event.target['amount-candle'].value;

        if (!this.dblClickEvent) this.allowCreate = true;
        else this.newCandle(this.dblClickEvent.x, this.dblClickEvent.y);

        event.target.reset();
    }

    closeModal(target) {
        if (!target) {
            Array.from(document.getElementsByClassName("modal"))
                .forEach(modal => modal.classList.remove("show"));
        } else target.closest('.modal').classList.remove('show');

        document.getElementById('search-candle').classList.remove('error');
    }

    showModalAddCandle() {
        this.closeModal();
        document.getElementById("modal-add-candle").classList.add("show");
        document.getElementById("name-candle").focus();
    }

    showModalSearch() {
        this.closeModal();
        document.getElementById("modal-search").classList.add("show");
        document.getElementById("id-candle").focus();
    }

    newCandle(x, y) {
        this.mouse.x = (x / this.viewer.renderer.domElement.offsetWidth) * 2 - 1;
        this.mouse.y = -(y / this.viewer.renderer.domElement.offsetHeight) * 2 + 1;

        this.raycaster.setFromCamera(this.mouse, this.viewer.camera);
        const intersects = this.raycaster.intersectObjects([this.planeComponent.plane]);

        if (intersects.length) {
            this.planeComponent.addСandle(intersects[0], this.newCandleData, false);
            this.allowCreate = false;
            this.newCandleData = null;
            this.dblClickEvent = null;
        }
    }
}
